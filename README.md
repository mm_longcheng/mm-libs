##compile

1.asm tools
add tools to PATH 
```
tools/asm/nasm-2.13.02
tools/asm/vsyasm-1.3.0-win64
```

cp /tools/asm/asm-visual-studio/* to
windows  7  2012 Program Files (x86)\MSBuild\Microsoft.Cpp\v4.0\V110\BuildCustomizations
windows 10  2017 2017\Community\Common7\IDE\VC\VCTargets\BuildCustomizations

2.ENV
```
MM_HOME=target to mm directory
POWERVR_SDK_HOME=target to you last version PowerVR_SDK directory
	some like E:\tools\Imagination\PowerVR_Graphics\PowerVR_SDK\SDK_2017_R1
	or low version at sdk\PowerVR\GraphicsSDK\SDK_3.3
```

3.sdk is platform special lib.

4.script
script\compile\compile_* for compile.

5. Visual Studio
```
VS141COMNTOOLS=*\2017\Community\VC\Auxiliary\Build\
like   VS141COMNTOOLS=F:\visual-studio\2017\Community\VC\Auxiliary\Build\
```

6. xcode
```
sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer
xcodebuild -version
sudo gem install xcpretty
```

